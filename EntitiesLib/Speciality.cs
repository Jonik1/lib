﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesLib
{
    public class Speciality
    {
        [Key]
        public int Id { get; set; }
        public int IdCathedra { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
