﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesLib
{
    public class RPD
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
        public int IdTutor { get; set; }
        public int IdCathedra { get; set; }
        public int IdPlan { get; set; }
        public int IdSubject { get; set; }
    }
}
