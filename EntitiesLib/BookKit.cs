﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesLib
{
    public class BookKit
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int idBook { get; set; }
        public int IdRpd { get; set; }
        public int IdEducation { get; set; }
        public int Year { get; set; }
        public int CountStud { get; set; }
        public int CountBook { get; set; }
    }
}
