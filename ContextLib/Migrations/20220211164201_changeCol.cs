﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ContextLib.Migrations
{
    public partial class changeCol : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Code",
                table: "Profiles");

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "Specialities",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Code",
                table: "Specialities");

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "Profiles",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }
    }
}
