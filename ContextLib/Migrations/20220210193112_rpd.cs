﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ContextLib.Migrations
{
    public partial class rpd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IdSubject",
                table: "Plans");

            migrationBuilder.AddColumn<int>(
                name: "IdSubject",
                table: "RPDs",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IdSubject",
                table: "RPDs");

            migrationBuilder.AddColumn<int>(
                name: "IdSubject",
                table: "Plans",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
