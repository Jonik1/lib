﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ContextLib.Migrations
{
    public partial class vers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Employees");

            migrationBuilder.DropTable(
                name: "LogBooks");

            migrationBuilder.DropColumn(
                name: "IdEducation",
                table: "Specialities");

            migrationBuilder.RenameColumn(
                name: "Id_Ticket",
                table: "Students",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "IdFaculty",
                table: "Specialities",
                newName: "IdCathedra");

            migrationBuilder.RenameColumn(
                name: "IdSpeciality",
                table: "Plans",
                newName: "IdSubject");

            migrationBuilder.RenameColumn(
                name: "IdSpeciality",
                table: "Groups",
                newName: "IdProfile");

            migrationBuilder.RenameColumn(
                name: "IdEducation",
                table: "Books",
                newName: "idEducation");

            migrationBuilder.RenameColumn(
                name: "Subject",
                table: "BookKits",
                newName: "Year");

            migrationBuilder.RenameColumn(
                name: "IdPlan",
                table: "BookKits",
                newName: "IdRpd");

            migrationBuilder.AddColumn<int>(
                name: "IdFgos",
                table: "Subjects",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "TicketNumber",
                table: "Students",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "IdPlan",
                table: "Groups",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "ISBN",
                table: "Books",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "CountBook",
                table: "BookKits",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CountStud",
                table: "BookKits",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "IdEducation",
                table: "BookKits",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Cathedras",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IdFaculty = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cathedras", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FGOSs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FGOSs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Profiles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IdSpeciality = table.Column<int>(type: "int", nullable: false),
                    IdEducation = table.Column<int>(type: "int", nullable: false),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Profiles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RPDs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Year = table.Column<int>(type: "int", nullable: false),
                    IdTutor = table.Column<int>(type: "int", nullable: false),
                    IdCathedra = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RPDs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tutors",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SecondName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Patronymic = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Birth = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tutors", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Cathedras");

            migrationBuilder.DropTable(
                name: "FGOSs");

            migrationBuilder.DropTable(
                name: "Profiles");

            migrationBuilder.DropTable(
                name: "RPDs");

            migrationBuilder.DropTable(
                name: "Tutors");

            migrationBuilder.DropColumn(
                name: "IdFgos",
                table: "Subjects");

            migrationBuilder.DropColumn(
                name: "TicketNumber",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "IdPlan",
                table: "Groups");

            migrationBuilder.DropColumn(
                name: "CountBook",
                table: "BookKits");

            migrationBuilder.DropColumn(
                name: "CountStud",
                table: "BookKits");

            migrationBuilder.DropColumn(
                name: "IdEducation",
                table: "BookKits");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Students",
                newName: "Id_Ticket");

            migrationBuilder.RenameColumn(
                name: "IdCathedra",
                table: "Specialities",
                newName: "IdFaculty");

            migrationBuilder.RenameColumn(
                name: "IdSubject",
                table: "Plans",
                newName: "IdSpeciality");

            migrationBuilder.RenameColumn(
                name: "IdProfile",
                table: "Groups",
                newName: "IdSpeciality");

            migrationBuilder.RenameColumn(
                name: "idEducation",
                table: "Books",
                newName: "IdEducation");

            migrationBuilder.RenameColumn(
                name: "Year",
                table: "BookKits",
                newName: "Subject");

            migrationBuilder.RenameColumn(
                name: "IdRpd",
                table: "BookKits",
                newName: "IdPlan");

            migrationBuilder.AddColumn<int>(
                name: "IdEducation",
                table: "Specialities",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "ISBN",
                table: "Books",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Patronymic = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    SecondName = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LogBooks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateEnd = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DateStart = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IdEmployee = table.Column<int>(type: "int", nullable: false),
                    IdStudent = table.Column<int>(type: "int", nullable: false),
                    idBook = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LogBooks", x => x.Id);
                });
        }
    }
}
