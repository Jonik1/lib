﻿using Microsoft.EntityFrameworkCore;
using EntitiesLib;

namespace ContextLib
{
    public partial class ContextDb : DbContext
    {
        public ContextDb()
        {
            //Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=localhost\SQLEXPRESS;Database=VKR2;Trusted_Connection=True;");
        }

        public DbSet<Book> Books { get; set; }
        public DbSet<BookKit> BookKits { get; set; }
        public DbSet<Cathedra> Cathedras { get; set; }
        public DbSet<Education> Educations { get; set; }
        public DbSet<Faculty> Faculties { get; set; }
        public DbSet<FGOS>  FGOSs { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Plan> Plans { get; set; }
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<RPD> RPDs { get; set; }
        public DbSet<Speciality> Specialities { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Tutor> Tutors { get; set; }
    }
}