﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ContextLib;
using EntitiesLib;

namespace Lib
{
    public static class DataBase
    {
        public static List<Book> GetBooks()
        {
            using (ContextDb db = new ContextDb())
            {
                return db.Books.ToList();
            }
        }
        public static List<BookKit> GetBookKits()
        {
            using (ContextDb db = new ContextDb())
            {
                return db.BookKits.ToList();
            }
        }
        public static List<Cathedra> GetCathedra()
        {
            using (ContextDb db = new ContextDb())
            {
                return db.Cathedras.ToList();
            }
        }
        public static List<Education> GetEducations()
        {
            using (ContextDb db = new ContextDb())
            {
                return db.Educations.ToList();
            }
        }   
        public static List<Faculty> GetFaculties()
        {
            using (ContextDb db = new ContextDb())
            {
                return db.Faculties.ToList();
            }
        }
        public static List<FGOS> GetFGOs()
        {
            using (ContextDb db = new ContextDb())
            {
                return db.FGOSs.ToList();
            }
        }
        public static List<Group> GetGroups()
        {
            using (ContextDb db = new ContextDb())
            {
                return db.Groups.ToList();
            }
        }
        public static List<Plan> GetPlans()
        {
            using (ContextDb db = new ContextDb())
            {
                return db.Plans.ToList();
            }
        }
        public static List<Profile> GetProfiles()
        {
            using (ContextDb db = new ContextDb())
            {
                return db.Profiles.ToList();
            }
        }
        public static List<RPD> GetRPDs()
        {
            using (ContextDb db = new ContextDb())
            {
                return db.RPDs.ToList();
            }
        }
        public static List<Speciality> GetSpecialities()
        {
            using (ContextDb db = new ContextDb())
            {
                return db.Specialities.ToList();
            }
        }
        public static List<Student> GetStudents()
        {
            using (ContextDb db = new ContextDb())
            {
                return db.Students.ToList();
            }
        }
        public static List<Subject> GetSubjects()
        {
            using (ContextDb db = new ContextDb())
            {
                return db.Subjects.ToList();
            }
        }
        public static List<Tutor> GetTutors()
        {
            using (ContextDb db = new ContextDb())
            {
                return db.Tutors.ToList();
            }
        }
    }
}
