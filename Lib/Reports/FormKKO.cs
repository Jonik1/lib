﻿using LiveCharts;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lib.Reports
{
    public partial class FormKKO : Form
    {
        public FormKKO()
        {
            InitializeComponent();
            var Ki = 0.2m;
            var fac = DataBase.GetFaculties();
            var data = new List<KKODBO>();
            foreach (var item in fac)
            {
                var cath = DataBase.GetCathedra().Where(x=> x.IdFaculty == item.Id).ToList();
                foreach (var item2 in cath)
                {
                    var spec = DataBase.GetSpecialities().Where(x => x.IdCathedra == item2.Id).ToList();
                    foreach (var item3 in spec)
                    {
                        var countProf = 0m;
                        var KKOProf = 0m;
                        var prof = DataBase.GetProfiles().Where(x => x.IdSpeciality == item3.Id).ToList();
                        foreach (var item4 in prof)
                        {
                            //считаем количество студентов
                            var countGroup = 0m;
                            var KKOGroup = 0m;

                            var grs = DataBase.GetGroups().Where(x => x.IdProfile == item4.Id).ToList();
                            foreach (var item5 in grs)
                            {
                                var countStud = DataBase.GetStudents().Where(x => x.IdGroup == item5.Id).Count();

                                var el = 0m;
                                var pr = 0m;
                                var elcnt = 0;
                                var prcnt = 0;

                                // выбираем количество книг
                                var dictBook = new Dictionary<int, decimal>();
                                var pls = DataBase.GetPlans().Where(x => x.Id == item5.IdPlan).FirstOrDefault();
                                if (pls != null)
                                {
                                    var rpd = DataBase.GetRPDs().Where(x => x.IdPlan == pls.Id).ToList();
                                    foreach (var item6 in rpd)
                                    {
                                        var kits = DataBase.GetBookKits().Where(x => x.IdRpd == item6.Id).ToList();
                                        foreach (var item7 in kits)
                                        {
                                            var books = DataBase.GetBooks().Where(x => x.Id == item7.idBook).FirstOrDefault();
                                            if (books != null)
                                            {
                                                if ((Electronic)books.Electronic == Electronic.Электронная)
                                                {
                                                    el += (decimal)1 * Ki;
                                                    elcnt++;
                                                }
                                                else
                                                {
                                                    pr += (decimal)books.Count / countStud;
                                                    prcnt++;
                                                }

                                            }
                                        }

                                    }
                                    if (elcnt != 0 || prcnt != 0)
                                    {
                                        if (elcnt != 0)
                                        {
                                            KKOGroup += (decimal)el / elcnt;
                                        }
                                        if (prcnt != 0)
                                        {
                                            KKOGroup += (decimal)pr / prcnt;
                                        }
                                        countGroup++;
                                    }
                                }
                            }
                            if (countGroup != 0)
                            {
                                countProf++;
                                KKOProf += (decimal)(KKOGroup / countGroup);
                            }
                        }
                        if (countProf != 0)
                        {
                            data.Add(new KKODBO()
                            {
                                FacName = item.Name,
                                SpecName = item3.Name,
                                KKO = KKOProf / countProf,
                            });
                        }

                    }
                }
            }
            dataGridView1.DataSource = data;

            var saa = data.Select(x => x.FacName + " " + x.SpecName).Distinct().ToArray();
            cartesianChart1.AxisX.Add(new Axis
            {
                Title = "Факультет",
                Labels = saa
            });
            cartesianChart1.AxisY.Add(new Axis
            {
                Title = "Величина ККО",
                LabelFormatter = value => Math.Round(value, 2).ToString()
            });
            cartesianChart1.LegendLocation = LegendLocation.Right;

            var series = new SeriesCollection();
            var line = data.Select(x => x.FacName + " " + x.SpecName).Distinct().ToList();
            foreach (var item in line)
            {
                List<decimal> lis = data.Where(x => x.FacName + " " + x.SpecName == item)
                    .Select(x => Math.Round(x.KKO, 2)).ToList();
                series.Add(new LineSeries()
                {
                    Title = item,
                    Values = new ChartValues<decimal>(lis),
                    LineSmoothness = 0
                });
            }
            cartesianChart1.Series = series;
        }
    }
}
