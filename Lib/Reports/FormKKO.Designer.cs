﻿namespace Lib.Reports
{
    partial class FormKKO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.facNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.specNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kKODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kKODBOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.cartesianChart1 = new LiveCharts.WinForms.CartesianChart();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kKODBOBindingSource)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(981, 552);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(973, 524);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "СписокККО";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.facNameDataGridViewTextBoxColumn,
            this.specNameDataGridViewTextBoxColumn,
            this.kKODataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.kKODBOBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(6, 6);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 25;
            this.dataGridView1.Size = new System.Drawing.Size(961, 512);
            this.dataGridView1.TabIndex = 0;
            // 
            // facNameDataGridViewTextBoxColumn
            // 
            this.facNameDataGridViewTextBoxColumn.DataPropertyName = "FacName";
            this.facNameDataGridViewTextBoxColumn.HeaderText = "FacName";
            this.facNameDataGridViewTextBoxColumn.Name = "facNameDataGridViewTextBoxColumn";
            this.facNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // specNameDataGridViewTextBoxColumn
            // 
            this.specNameDataGridViewTextBoxColumn.DataPropertyName = "SpecName";
            this.specNameDataGridViewTextBoxColumn.HeaderText = "SpecName";
            this.specNameDataGridViewTextBoxColumn.Name = "specNameDataGridViewTextBoxColumn";
            this.specNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // kKODataGridViewTextBoxColumn
            // 
            this.kKODataGridViewTextBoxColumn.DataPropertyName = "KKO";
            this.kKODataGridViewTextBoxColumn.HeaderText = "KKO";
            this.kKODataGridViewTextBoxColumn.Name = "kKODataGridViewTextBoxColumn";
            this.kKODataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // kKODBOBindingSource
            // 
            this.kKODBOBindingSource.DataSource = typeof(Lib.KKODBO);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.cartesianChart1);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(973, 524);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "График";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // cartesianChart1
            // 
            this.cartesianChart1.Location = new System.Drawing.Point(6, 6);
            this.cartesianChart1.Name = "cartesianChart1";
            this.cartesianChart1.Size = new System.Drawing.Size(971, 522);
            this.cartesianChart1.TabIndex = 1;
            this.cartesianChart1.Text = "cartesianChart1";
            // 
            // FormKKO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1005, 576);
            this.Controls.Add(this.tabControl1);
            this.Name = "FormKKO";
            this.Text = "FormKKO";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kKODBOBindingSource)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private TabControl tabControl1;
        private TabPage tabPage1;
        private DataGridViewTextBoxColumn grYearDataGridViewTextBoxColumn;
        private TabPage tabPage2;
        private LiveCharts.WinForms.CartesianChart cartesianChart1;
        private DataGridView dataGridView1;
        private DataGridViewTextBoxColumn facNameDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn specNameDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn kKODataGridViewTextBoxColumn;
        private BindingSource kKODBOBindingSource;
    }
}