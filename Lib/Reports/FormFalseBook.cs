﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lib.Reports
{
    public partial class FormFalseBook : Form
    {
        public FormFalseBook()
        {
            InitializeComponent();
            var data = new List<FailedBooksDTO>(); 
            var prof = DataBase.GetProfiles();
            foreach (var item in prof)
            {
                var spec = DataBase.GetSpecialities().Where(x => item.IdSpeciality == x.Id).FirstOrDefault();
                if (spec != null)
                {
                    var cath = DataBase.GetCathedra().Where(x => spec.IdCathedra == x.Id).FirstOrDefault();
                    if (cath != null)
                    {
                        var rpds = DataBase.GetRPDs().Where(x => x.IdCathedra == cath.Id).ToList();
                        foreach (var rpd in rpds)
                        {
                            var kits = DataBase.GetBookKits().Where(x => x.IdRpd == rpd.Id).ToList();
                            foreach (var kit in kits)
                            {
                                var books = DataBase.GetBooks().Where(x => x.Id == kit.idBook).FirstOrDefault();
                                if (books != null)
                                {
                                    if (books.idEducation != kit.IdEducation)
                                    {
                                        data.Add(new FailedBooksDTO()
                                        {
                                            BookAuthor = books.Author,
                                            BookName = books.Name,
                                            ProfName = item.Name,
                                            CathedraName = cath.Name,
                                            SpecName = spec.Name,
                                            RptYear = rpd.Year,
                                        }
                                        );
                                    }
                                }
                            }
                        }
                    }
                }
            }
            dataGridView1.DataSource = data;
        }
    }
}
