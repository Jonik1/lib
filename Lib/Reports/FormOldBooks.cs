﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lib.Reports
{
    public partial class FormOldBooks : Form
    {
        public FormOldBooks()
        {
            InitializeComponent();
            var data = new List<BookDTO>();
            var dbElem = DataBase.GetBooks().Where(x=>(DateTime.Now.Year -x.Year  )>=4);

            foreach (var elem in dbElem)
            {
                var education = DataBase.GetEducations().
                    Where(x => x.Id == elem.idEducation).FirstOrDefault();
                data.Add(new BookDTO
                {
                    Author = elem.Author,
                    BookType = ((BookType)elem.BookType).ToString(),
                    Count = elem.Count,
                    idEducation = education != null ? education.Name : "",
                    Electronic = ((Electronic)elem.Electronic).ToString(),
                    Id = elem.Id,
                    ISBN = elem.ISBN,
                    Name = elem.Name,
                    Year = elem.Year
                });
            }

            dataGridView1.DataSource = data;
        }

        private void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            foreach (DataGridViewRow elem in dataGridView1.Rows)
            {
                if ((DateTime.Now.Year - (int)elem.Cells[3].Value) == 4)
                    elem.DefaultCellStyle.BackColor = Color.Yellow;
                else
                    elem.DefaultCellStyle.BackColor = Color.Red;
            }
        }
    }
}
