﻿namespace Lib.Reports
{
    partial class FormFalseBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.failedBooksDTOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cathedraNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.specNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.profNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rptYearDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bookNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bookAuthorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.failedBooksDTOBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cathedraNameDataGridViewTextBoxColumn,
            this.specNameDataGridViewTextBoxColumn,
            this.profNameDataGridViewTextBoxColumn,
            this.rptYearDataGridViewTextBoxColumn,
            this.bookNameDataGridViewTextBoxColumn,
            this.bookAuthorDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.failedBooksDTOBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 25;
            this.dataGridView1.Size = new System.Drawing.Size(776, 426);
            this.dataGridView1.TabIndex = 0;
            // 
            // failedBooksDTOBindingSource
            // 
            this.failedBooksDTOBindingSource.DataSource = typeof(Lib.FailedBooksDTO);
            // 
            // cathedraNameDataGridViewTextBoxColumn
            // 
            this.cathedraNameDataGridViewTextBoxColumn.DataPropertyName = "CathedraName";
            this.cathedraNameDataGridViewTextBoxColumn.HeaderText = "CathedraName";
            this.cathedraNameDataGridViewTextBoxColumn.Name = "cathedraNameDataGridViewTextBoxColumn";
            this.cathedraNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // specNameDataGridViewTextBoxColumn
            // 
            this.specNameDataGridViewTextBoxColumn.DataPropertyName = "SpecName";
            this.specNameDataGridViewTextBoxColumn.HeaderText = "SpecName";
            this.specNameDataGridViewTextBoxColumn.Name = "specNameDataGridViewTextBoxColumn";
            this.specNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // profNameDataGridViewTextBoxColumn
            // 
            this.profNameDataGridViewTextBoxColumn.DataPropertyName = "ProfName";
            this.profNameDataGridViewTextBoxColumn.HeaderText = "ProfName";
            this.profNameDataGridViewTextBoxColumn.Name = "profNameDataGridViewTextBoxColumn";
            this.profNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // rptYearDataGridViewTextBoxColumn
            // 
            this.rptYearDataGridViewTextBoxColumn.DataPropertyName = "RptYear";
            this.rptYearDataGridViewTextBoxColumn.HeaderText = "RptYear";
            this.rptYearDataGridViewTextBoxColumn.Name = "rptYearDataGridViewTextBoxColumn";
            this.rptYearDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bookNameDataGridViewTextBoxColumn
            // 
            this.bookNameDataGridViewTextBoxColumn.DataPropertyName = "BookName";
            this.bookNameDataGridViewTextBoxColumn.HeaderText = "BookName";
            this.bookNameDataGridViewTextBoxColumn.Name = "bookNameDataGridViewTextBoxColumn";
            this.bookNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // bookAuthorDataGridViewTextBoxColumn
            // 
            this.bookAuthorDataGridViewTextBoxColumn.DataPropertyName = "BookAuthor";
            this.bookAuthorDataGridViewTextBoxColumn.HeaderText = "BookAuthor";
            this.bookAuthorDataGridViewTextBoxColumn.Name = "bookAuthorDataGridViewTextBoxColumn";
            this.bookAuthorDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // FormFalseBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dataGridView1);
            this.Name = "FormFalseBook";
            this.Text = "FormFalseBook";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.failedBooksDTOBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataGridView dataGridView1;
        private DataGridViewTextBoxColumn cathedraNameDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn specNameDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn profNameDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn rptYearDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn bookNameDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn bookAuthorDataGridViewTextBoxColumn;
        private BindingSource failedBooksDTOBindingSource;
    }
}