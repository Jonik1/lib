﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lib.DictForms
{
    public partial class FormEducation : Form
    {
        public FormEducation()
        {
            InitializeComponent();
            var data = new List<EducationDTO>();
            var dbElem = DataBase.GetEducations();

            foreach (var elem in dbElem)
            {
                data.Add(new EducationDTO
                {
                    Id = elem.Id,
                    Name = elem.Name,
                });
            }

            dataGridView1.DataSource = data;
        }
    }
}
