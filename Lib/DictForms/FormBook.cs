﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lib.DictForms
{
    public partial class FormBook : Form
    {
        public FormBook()
        {
            InitializeComponent();
            var data = new List<BookDTO>();
            var dbElem = DataBase.GetBooks();

            foreach (var elem in dbElem)
            {
                data.Add(new BookDTO
                {
                    Author = elem.Author,
                    BookType = ((BookType)elem.BookType).ToString(),
                    Count = elem.Count,
                    Electronic = ((Electronic)elem.Electronic).ToString(),
                    Id = elem.Id,
                    ISBN = elem.ISBN,
                    Name = elem.Name,
                    Year = elem.Year
                });
            }
            dataGridView1.DataSource= data;
        }
    }
}