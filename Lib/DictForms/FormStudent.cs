﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lib.DictForms
{
    public partial class FormStudent : Form
    {
        public FormStudent()
        {
            InitializeComponent();
            var data = new List<StudentDTO>();
            var dbElem = DataBase.GetStudents();

            foreach (var elem in dbElem)
            {
                var group = DataBase.GetGroups().
                    Where(x => x.Id == elem.IdGroup).FirstOrDefault();

                data.Add(new StudentDTO
                {
                    Id = elem.Id,
                    TicketNumber = elem.TicketNumber,
                    Name = elem.Name,
                    IdGroup = group != null ? group.Name:"",
                    Patronymic = elem.Patronymic,
                    SecondName = elem.SecondName,
                });
            }

            dataGridView1.DataSource = data;
        }
    }
}
