﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lib.DictForms
{
    public partial class FormFGOS : Form
    {
        public FormFGOS()
        {
            InitializeComponent();
            var data = new List<FGOSDTO>();
            var dbElem = DataBase.GetFGOs();

            foreach (var elem in dbElem)
            {
                data.Add(new FGOSDTO
                {
                    Id = elem.Id,
                    Name = elem.Name,
                });
            }

            dataGridView1.DataSource = data;
        }
    }
}
