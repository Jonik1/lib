﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lib.DictForms
{
    public partial class FormRPD : Form
    {
        public FormRPD()
        {
            InitializeComponent();
            var data = new List<RPDDTO>();
            var dbElem = DataBase.GetRPDs();

            foreach (var elem in dbElem)
            {
                var cath = DataBase.GetCathedra().Where(x => x.Id == elem.IdCathedra).FirstOrDefault();
                var tut = DataBase.GetTutors().Where(x => x.Id == elem.IdTutor).FirstOrDefault();
                var plan = DataBase.GetPlans().Where(x => x.Id == elem.IdPlan).FirstOrDefault();
                var subj = DataBase.GetSubjects().Where(x => x.Id == elem.IdSubject).FirstOrDefault();
                data.Add(new RPDDTO
                {
                    Id = elem.Id,
                    Name = elem.Name,
                    IdCathedra = cath != null ? cath.Name : "",
                    IdTutor = tut != null ? tut.SecondName + tut.Name : "",
                    Year = elem.Year,
                    IdPlan = plan != null ? plan.Name : "",
                    IdSubject = subj != null ? subj.Name : "",
                });
            }
            dataGridView1.DataSource = data;
        }
    }
}
