﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lib.DictForms
{
    public partial class FormBookKit : Form
    {
        public FormBookKit()
        {
            InitializeComponent();
            var data = new List<BookKitDTO>();
            var dbElem = DataBase.GetBookKits();

            foreach (var elem in dbElem)
            {
                var book = DataBase.GetBooks().
                    Where(x => x.Id == elem.idBook).FirstOrDefault();
                var ed = DataBase.GetEducations().
                    Where(x => x.Id == elem.IdEducation).FirstOrDefault();
                var rpd = DataBase.GetRPDs().
                                    Where(x => x.Id == elem.IdRpd).FirstOrDefault();
                data.Add(new BookKitDTO
                {
                    Id = elem.Id,
                    idBook = book != null ? book.Name : "",
                    Name = elem.Name,
                    IdEducation = ed != null ? ed.Name : "",
                    IdRpd = rpd != null ? rpd.Name : "",
                    Year = elem.Year,
                    CountBook = elem.CountBook,
                    CountStud = elem.CountStud,
                });
            }
            dataGridView1.DataSource = data;
        }
    }
}
