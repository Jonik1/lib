﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lib.DictForms
{
    public partial class FormGroup : Form
    {
        public FormGroup()
        {
            InitializeComponent();
            var data = new List<GroupDTO>();
            var dbElem = DataBase.GetGroups();

            foreach (var elem in dbElem)
            {
                var plan = DataBase.GetPlans().Where(x => x.Id == elem.IdPlan).FirstOrDefault();
                var profile = DataBase.GetProfiles().Where(x => x.Id == elem.IdProfile).FirstOrDefault();
                data.Add(new GroupDTO
                {
                    Id = elem.Id,
                    Name = elem.Name,
                    IdPlan = plan != null ? plan.Name : "",
                    IdProfile = profile != null ? profile.Name : "",
                    Year = elem.Year,
                });
            }
            dataGridView1.DataSource = data;
        }
    }
}
