﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lib.DictForms
{
    public partial class FormSubject : Form
    {
        public FormSubject()
        {
            InitializeComponent();
            var data = new List<SubjectDTO>();
            var dbElem = DataBase.GetSubjects();

            foreach (var elem in dbElem)
            {
                var fgos = DataBase.GetFGOs().
                    Where(x => x.Id == elem.IdFgos).FirstOrDefault();

                data.Add(new SubjectDTO
                {
                    Id = elem.Id,
                    Name = elem.Name,
                    IdFgos = fgos != null ? fgos.Name : ""
                });
            }

            dataGridView1.DataSource = data;
        }
    }
}
