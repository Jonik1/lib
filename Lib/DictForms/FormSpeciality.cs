﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lib.DictForms
{
    public partial class FormSpeciality : Form
    {
        public FormSpeciality()
        {
            InitializeComponent();
            var data = new List<SpecialityDTO>();
            var dbElem = DataBase.GetSpecialities();

            foreach (var elem in dbElem)
            {
                var cath = DataBase.GetCathedra().
                    Where(x => x.Id == elem.IdCathedra).FirstOrDefault();
                data.Add(new SpecialityDTO
                {
                    Id = elem.Id,
                    Name = elem.Name,
                    IdCathedra = cath != null ? cath.Name:"",
                    Code = elem.Code,
                });
            }

            dataGridView1.DataSource = data;
        }
    }
}
