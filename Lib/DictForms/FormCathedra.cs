﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lib.DictForms
{
    public partial class FormCathedra : Form
    {
        public FormCathedra()
        {
            InitializeComponent();
            var data = new List<CathedraDTO>();
            var dbElem = DataBase.GetCathedra();

            foreach (var elem in dbElem)
            {
                var faculty = DataBase.GetFaculties().
                                    Where(x => x.Id == elem.IdFaculty).FirstOrDefault();
                data.Add(new CathedraDTO
                {
                    Id = elem.Id,
                    Name = elem.Name,
                    IdFaculty = faculty != null ? faculty.Name : ""
                });
            }

            dataGridView1.DataSource = data;
        }
    }
}
