﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lib.DictForms
{
    public partial class FormProfile : Form
    {
        public FormProfile()
        {
            InitializeComponent();
            var data = new List<ProfileDTO>();
            var dbElem = DataBase.GetProfiles();

            foreach (var elem in dbElem)
            {
                var edu = DataBase.GetEducations().Where(x => x.Id == elem.IdEducation).FirstOrDefault();
                var spec = DataBase.GetSpecialities().Where(x => x.Id == elem.IdSpeciality).FirstOrDefault();
                data.Add(new ProfileDTO
                {
                    Id = elem.Id,
                    Name = elem.Name,
                    IdEducation = edu != null ? edu.Name : "",
                    IdSpeciality = spec != null ? spec.Name : "",
                });
            }
            dataGridView1.DataSource = data;
        }
    }
}
