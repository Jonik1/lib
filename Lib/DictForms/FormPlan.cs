﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lib.DictForms
{
    public partial class FormPlan : Form
    {
        public FormPlan()
        {
            InitializeComponent();
            var data = new List<PlanDTO>();
            var dbElem = DataBase.GetPlans();

            foreach (var elem in dbElem)
            {
                data.Add(new PlanDTO
                {
                    Id = elem.Id,
                    Name = elem.Name,
                    Year = elem.Year
                });
            }
            dataGridView1.DataSource = data;
        }
    }
}
