﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lib.DictForms
{
    public partial class FormFaculty : Form
    {
        public FormFaculty()
        {
            InitializeComponent();
            var data = new List<FacultyDTO>();
            var dbElem = DataBase.GetFaculties();

            foreach (var elem in dbElem)
            {
                data.Add(new FacultyDTO
                {
                    Id = elem.Id,
                    Name = elem.Name
                });
            }
            dataGridView1.DataSource = data;
        }
    }
}
