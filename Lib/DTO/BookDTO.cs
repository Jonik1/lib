﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib
{
    enum BookType
    {
        Основная = 0,
        Дополнительная = 1
    }
    enum Electronic
    {
        Печатная = 0,
        Электронная = 1
    }

    public class BookDTO
    {
        public int Id { get; set; }
        public string ISBN { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
        public string Author { get; set; }
        public int Count { get; set; }
        public string BookType { get; set; }
        public string Electronic { get; set; }
        public string idEducation { get; set; }
    }
}
