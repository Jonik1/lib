﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib
{
    public class RPDDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
        public string IdTutor { get; set; }
        public string IdCathedra { get; set; }
        public string IdPlan { get; set; }
        public string IdSubject { get; set; }
    }
}
