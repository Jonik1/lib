﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib
{
    public class FailedBooksDTO
    {
        public string CathedraName { get; set; }
        public string SpecName { get; set; }
        public string ProfName{ get; set; }
        public int RptYear { get; set; }
        public string BookName { get; set; }
        public string BookAuthor { get; set; }
    }
}
