﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib
{
    public class BookKitDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string idBook { get; set; }
        public string IdRpd { get; set; }
        public string IdEducation { get; set; }
        public int Year { get; set; }
        public int CountStud { get; set; }
        public int CountBook { get; set; }
    }
}
