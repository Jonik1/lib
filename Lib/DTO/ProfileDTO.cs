﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib
{
    public class ProfileDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string IdSpeciality { get; set; }
        public string IdEducation { get; set; }

    }
}
