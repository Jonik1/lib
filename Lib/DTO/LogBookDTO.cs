﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib
{
    public class LogBookDTO
    {

        public int Id { get; set; }
        public int IdStudent { get; set; }
        public int IdEmployee { get; set; }
        public int idBook { get; set; }
        public DateTime? DateStart { get; set; }
        public DateTime? DateEnd { get; set; }
    }
}
