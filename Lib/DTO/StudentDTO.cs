﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib
{
    public class StudentDTO
    {
        public int Id { get; set; }
        public string IdGroup { get; set; }
        public string TicketNumber { get; set; }
        public string Name { get; set; }
        public string SecondName { get; set; }
        public string Patronymic { get; set; }
    }
}
