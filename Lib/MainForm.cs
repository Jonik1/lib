using ContextLib;
using Lib.DictForms;
using Lib.Reports;

namespace Lib
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            comboBox1.Items.AddRange(DataBase.GetFaculties().Select(x => x.Name).ToArray());
            comboBox7.Items.AddRange(DataBase.GetEducations().Select(x => x.Name).ToArray());
            comboBox6.Items.AddRange( new string[] { Electronic.��������.ToString(), Electronic.�����������.ToString() });
            comboBox5.Items.AddRange(new string[] { BookType.��������.ToString(), BookType.��������������.ToString() });
            comboBox9.Items.AddRange(DataBase.GetSubjects().Select(x => x.Name).ToArray());
        }

        private void FacultyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FormFaculty().Show();
        }

        private void Speciality_Click(object sender, EventArgs e)
        {
            new FormSpeciality().Show();
        }

        private void Book_Click(object sender, EventArgs e)
        {
            new FormBook().Show();
        }

        private void Group_Click(object sender, EventArgs e)
        {
            new FormGroup().Show();
        }

        private void Student_Click(object sender, EventArgs e)
        {
            new FormStudent().Show();
        }

        private void Plan_Click(object sender, EventArgs e)
        {
            new FormPlan().Show();
        }

        private void Employee_Click(object sender, EventArgs e)
        {
            new FormTutor().Show();
        }

        private void SubjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FormSubject().Show();
        }

        private void BookKitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FormBookKit().Show();
        }

        private void OldBooksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FormOldBooks().Show();
        }
        private void CathedraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FormCathedra().Show();
        }

        private void FGOSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FormFGOS().Show();
        }

        private void ProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FormProfile().Show();
        }

        private void RPDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FormRPD().Show();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var fac = DataBase.GetFaculties().Where(x => x.Name == comboBox1.SelectedItem.ToString()).FirstOrDefault();
            var cath = DataBase.GetCathedra().Where(x => x.IdFaculty == fac.Id).Select(x => x.Name).ToArray();
            comboBox2.Items.Clear();
            comboBox2.Items.AddRange(cath);
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cath = DataBase.GetCathedra().Where(x => x.Name == comboBox2.SelectedItem.ToString()).FirstOrDefault();
            var spec = DataBase.GetSpecialities().Where(x => x.IdCathedra == cath.Id).Select(x => x.Name).ToArray();
            comboBox3.Items.Clear();
            comboBox3.Items.AddRange(spec);
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            var spec = DataBase.GetSpecialities().Where(x => x.Name == comboBox3.SelectedItem.ToString()).FirstOrDefault();
            var prof = DataBase.GetProfiles().Where(x => x.IdSpeciality == spec.Id).Select(x => x.Name).ToArray();
            comboBox4.Items.Clear();
            comboBox4.Items.AddRange(prof);
        }
        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            var prof = DataBase.GetProfiles().Where(x => x.Name == comboBox4.SelectedItem.ToString()).FirstOrDefault();
            var gr = DataBase.GetGroups().Where(x => x.IdProfile == prof.Id).Select(x => x.Name).ToArray();
            comboBox8.Items.Clear();
            comboBox8.Items.AddRange(gr);
        }

        private void comboBox8_SelectedIndexChanged(object sender, EventArgs e)
        {
            var gr = DataBase.GetGroups().Where(x => x.Name == comboBox8.SelectedItem.ToString()).FirstOrDefault();
            var std = DataBase.GetStudents().Where(x => x.IdGroup == gr.Id).ToList();
            dataGridView1.DataSource = std;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var books = DataBase.GetBooks();

            if (comboBox7.SelectedItem != null)
            {
                var ed = DataBase.GetEducations().Where(x => x.Name == comboBox7.SelectedItem.ToString()).FirstOrDefault();
                books = books.Where(x => x.idEducation == ed.Id).ToList();
            }
            if (comboBox6.SelectedItem != null)
            {
                var ed = comboBox6.SelectedItem.ToString();
                books = books.Where(x => ((Electronic)x.Electronic).ToString() == ed).ToList();
            }
            if (comboBox5.SelectedItem != null)
            {
                var ed = comboBox5.SelectedItem.ToString();
                books = books.Where(x => ((BookType)x.BookType).ToString() == ed).ToList();
            }
            if (!string.IsNullOrEmpty(textBox1.Text))
            {
                var txt = textBox1.Text;
                books = books.Where(x => x.Year.ToString().ToLower().Contains(txt.ToLower())
                    || x.Author.ToString().ToLower().Contains(txt.ToLower())
                    || x.ISBN.ToString().ToLower().Contains(txt.ToLower())
                    || x.Name.ToString().ToLower().Contains(txt.ToLower())
                    ).ToList();
            }
            var data = new List<BookDTO>();
            foreach (var elem in books)
            {
                var education = DataBase.GetEducations().
                    Where(x => x.Id == elem.idEducation).FirstOrDefault();
                data.Add(new BookDTO
                {
                    Author = elem.Author,
                    BookType = ((BookType)elem.BookType).ToString(),
                    Count = elem.Count,
                    idEducation = education != null ? education.Name : "",
                    Electronic = ((Electronic)elem.Electronic).ToString(),
                    Id = elem.Id,
                    ISBN = elem.ISBN,
                    Name = elem.Name,
                    Year = elem.Year
                });
            }
            dataGridView2.DataSource = data;
        }

        private void BookFalseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FormFalseBook().Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var books = DataBase.GetBooks();
            var data = new List<BookDTO>();
            foreach (var elem in books)
            {
                var education = DataBase.GetEducations().
                    Where(x => x.Id == elem.idEducation).FirstOrDefault();
                data.Add(new BookDTO
                {
                    Author = elem.Author,
                    BookType = ((BookType)elem.BookType).ToString(),
                    Count = elem.Count,
                    idEducation = education != null ? education.Name : "",
                    Electronic = ((Electronic)elem.Electronic).ToString(),
                    Id = elem.Id,
                    ISBN = elem.ISBN,
                    Name = elem.Name,
                    Year = elem.Year
                });
            }
            dataGridView2.DataSource = data;
        }

        private void KkoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FormKKO().Show();
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (comboBox9.SelectedItem != null && textBox2.Text != "") 
            {
                var studCount = Int32.Parse(textBox2.Text);
                if (studCount != 0)
                {
                    var disc = DataBase.GetSubjects().Where(x => x.Name == comboBox9.SelectedItem.ToString()).FirstOrDefault();
                    if (disc != null)
                    {
                        var rpds = DataBase.GetRPDs().Where(x => x.IdSubject == disc.Id).ToList();
                        var Ki = 0.2m;
                        var el = 0m;
                        var pr = 0m;
                        var elcnt = 0;
                        var prcnt = 0;
                        var sum = 0m;
                        foreach (var rpd in rpds)
                        {
                            var kits = DataBase.GetBookKits().Where(x => x.IdRpd == rpd.Id).ToList();
                            foreach (var kit in kits)
                            {
                                var book = DataBase.GetBooks().Where(x => x.Id == kit.idBook).FirstOrDefault();
                                if (book.Electronic == 0)
                                {
                                    pr += Math.Min((decimal)kit.CountBook / (decimal)studCount, 1);
                                    prcnt++;
                                }
                                else
                                {
                                    el += Ki;
                                    elcnt++;
                                }
                            }
                        }
                        sum = (prcnt != 0 ? pr / prcnt : 0) + Ki * elcnt;
                        label13.Text = Math.Min(sum, 1).ToString("0.00");
                    }
                    
                }
            }
        }
        private void label13_TextChanged(object sender, EventArgs e)
        {
            if (Decimal.Parse(label13.Text) < 0.5m)
            {
                label13.BackColor = Color.Red;
            }
            else {
                label13.BackColor = Color.Transparent;
            }
        }
    }
}